<?php

/**
 * This class contains hooks, that can be enabled in LocalConfig.*.php
 */
class KDEHooks {
    public static function onPersonalUrls(array &$personal_urls, Title $title, SkinTemplate $skin) {
        // remove link to login and anonlogin
        unset($personal_urls['login']);
        unset($personal_urls['anonlogin']);
    }

    public static function onChangeAuthenticationDataAudit( $req, $status ) {
    }

    public static function onSpecialPage_initList(&$list) {
        unset($list['ChangeCredentials']);
        unset($list['PasswordReset']);
    }

    public static function onGetPreferences($user, &$preferences) {
        unset($preferences['password']);
    }

    public static function onSpecialSearchResults($term, $titleMatches, $textMatches) {
        
    }
}
